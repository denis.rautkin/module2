using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Http.Headers;
using System.Reflection;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Program program = new Program();

            int totalTax;
            int companiesNumber;
            int tax;
            int companyRevenue;

            Console.WriteLine("Input total companies number: ");
            ParseInt(Console.ReadLine(), out companiesNumber);

            Console.WriteLine("Input current tax value: ");
            ParseInt(Console.ReadLine(), out tax);

            Console.WriteLine("Input company revenue: ");
            ParseInt(Console.ReadLine(), out companyRevenue);

            totalTax = program.GetTotalTax(companiesNumber, tax, companyRevenue);
            Console.WriteLine("Companies total tax is: {0}", totalTax);

            int age;
            string congratulationText;

            Console.WriteLine("Input your age: ");
            ParseInt(Console.ReadLine(), out age);

            congratulationText = program.GetCongratulation(age);
            Console.WriteLine(congratulationText);

            string first;
            string second;
            double multiplicationResult;

            Console.WriteLine("Input first double number: ");
            first = Console.ReadLine();

            Console.WriteLine("Input second double number: ");
            second = Console.ReadLine();

            multiplicationResult = program.GetMultipliedNumbers(first, second);
            Console.WriteLine("Result of multiplication is: {0}", multiplicationResult);

            Figures chosenFigure;
            Parameters chosenParameter;
            Dimensions dimensions = new Dimensions();

            Console.WriteLine("Choose figures type: ");

            foreach (Figures element in Enum.GetValues(typeof(Figures)))
            {
                Console.WriteLine("{0}) {1}", (int)element, element.ToString());
            }

            ParseEnum<Figures>(Console.ReadLine(), out chosenFigure);

            Console.WriteLine("You choosed a {0} figure.", chosenFigure.ToString());

            Console.WriteLine("Choose parameter for calculation: ");

            foreach (Parameters element in Enum.GetValues(typeof(Parameters)))
            {
                Console.WriteLine("{0}) {1}", (int)element, element.ToString());
            }

            ParseEnum<Parameters>(Console.ReadLine(), out chosenParameter);

            Console.WriteLine("You choosed a {0} parameter for calculation.", chosenParameter.ToString());

            switch (chosenFigure)
            {
                case Figures.Triangle:

                    if(chosenParameter == Parameters.Perimeter)
                    {
                        double firstSide;
                        double secondSide;
                        double thirdSide;
                        double perimeter;

                        Console.WriteLine("Input first side of {0}", chosenFigure.ToString());
                        ParseDouble(Console.ReadLine(), out firstSide);
                        dimensions.FirstSide = firstSide;

                        Console.WriteLine("Input second side of {0}", chosenFigure.ToString());
                        ParseDouble(Console.ReadLine(), out secondSide);
                        dimensions.SecondSide = secondSide;

                        Console.WriteLine("Input third side of {0}", chosenFigure.ToString());
                        ParseDouble(Console.ReadLine(), out thirdSide);
                        dimensions.ThirdSide = thirdSide;

                        perimeter = program.GetFigureValues(chosenFigure, chosenParameter, dimensions);
                        Console.WriteLine("Perimeter of {0} about {1}", chosenFigure.ToString(), perimeter);

                    }
                    else if(chosenParameter == Parameters.Square)
                    {
                        double firstSide;
                        double height;
                        double square;

                        Console.WriteLine("Input first side of {0}", chosenFigure.ToString());
                        ParseDouble(Console.ReadLine(), out firstSide);
                        dimensions.FirstSide = firstSide;

                        Console.WriteLine("Input height of {0}", chosenFigure.ToString());
                        ParseDouble(Console.ReadLine(), out height);
                        dimensions.Height = height;

                        square = program.GetFigureValues(chosenFigure, chosenParameter, dimensions);
                        Console.WriteLine("Square of {0} about {1}", chosenFigure.ToString(), square);
                    }
                    
                    break;

                case Figures.Rectangle:

                    if (chosenParameter == Parameters.Perimeter)
                    {
                        double firstSide;
                        double secondSide;
                        double perimeter;

                        Console.WriteLine("Input first side of {0}", chosenFigure.ToString());
                        ParseDouble(Console.ReadLine(), out firstSide);
                        dimensions.FirstSide = firstSide;

                        Console.WriteLine("Input second side of {0}", chosenFigure.ToString());
                        ParseDouble(Console.ReadLine(), out secondSide);
                        dimensions.SecondSide = secondSide;

                        perimeter = program.GetFigureValues(chosenFigure, chosenParameter, dimensions);
                        Console.WriteLine("Perimeter of {0} about {1}", chosenFigure.ToString(), perimeter);

                    }
                    else if (chosenParameter == Parameters.Square)
                    {
                        double firstSide;
                        double secondSide;
                        double square;

                        Console.WriteLine("Input first side of {0}", chosenFigure.ToString());
                        ParseDouble(Console.ReadLine(), out firstSide);
                        dimensions.FirstSide = firstSide;

                        Console.WriteLine("Input second side of {0}", chosenFigure.ToString());
                        ParseDouble(Console.ReadLine(), out secondSide);
                        dimensions.SecondSide = secondSide;

                        square = program.GetFigureValues(chosenFigure, chosenParameter, dimensions);
                        Console.WriteLine("Square of {0} about {1}", chosenFigure.ToString(), square);
                    }

                    break;

                case Figures.Circle:

                    double radius;
                    double result;

                    Console.WriteLine("Input radius of {0}", chosenFigure.ToString());
                    ParseDouble(Console.ReadLine(), out radius);
                    dimensions.Radius = radius;
                    result = program.GetFigureValues(chosenFigure, chosenParameter, dimensions);

                    if (chosenParameter == Parameters.Perimeter)
                    {

                        Console.WriteLine("Perimeter of {0} about {1}", chosenFigure.ToString(), result);

                    }
                    else if (chosenParameter == Parameters.Square)
                    {
                        Console.WriteLine("Square of {0} about {1}", chosenFigure.ToString(), result);
                    }

                    break;

                default:

                    break;
            }
        }

            public static void ParseInt(string stringValue, out int intValue)
        {
            bool result;
            result = Int32.TryParse(stringValue.Trim(), out intValue);
            
            if(result)
            {
                Console.WriteLine("Converted '{0}' to {1}.", stringValue, intValue);
            }
            else
            {
                Console.WriteLine("Unable to convert '{0}'.", stringValue);
            }
        }

        public static void ParseDouble(string stringValue, out double doubleValue)
        {
            bool result;
            NumberStyles numberStyle;
            CultureInfo cultureInfo;

            numberStyle = NumberStyles.Any;
            cultureInfo = CultureInfo.GetCultureInfo("ru-RU");

            result = Double.TryParse(stringValue.Trim(), numberStyle, cultureInfo, out doubleValue);

            if(result)
            {
                Console.WriteLine("Converted '{0}' to {1}.", stringValue, doubleValue);
            }
            else
            {
                cultureInfo = CultureInfo.GetCultureInfo("en-US");
                result = Double.TryParse(stringValue.Trim(), numberStyle, cultureInfo, out doubleValue);

                if(result)
                {
                    Double.TryParse(stringValue.Trim(), numberStyle, cultureInfo, out doubleValue);
                }
                else
                {
                    Console.WriteLine("Unable to convert '{0}'.", stringValue);
                }
            }
        }

        public static void ParseEnum<T>(string stringValue, out T enumValue)
            where T : struct
        {
            bool result;

            result = Enum.TryParse<T>(stringValue, true, out enumValue);

            if (result)
            {
                if(Enum.IsDefined(typeof(T), enumValue))
                {
                    Console.WriteLine("Converted '{0}' to {1}.", stringValue, enumValue.ToString());
                }
                else
                {
                    Console.WriteLine("{0} is not a value of the enum!");
                }
            }
            else
            {
                Console.WriteLine("{0} is not a member of the enum!");
            }
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            int totalTax;
            totalTax = (((companyRevenue * tax) * companiesNumber) / 100 );
            return totalTax;
        }

        public string GetCongratulation(int input)
        {
            string congratulationText;

            if (((input & 1) == 0) && (input >= 18))
            {
                congratulationText = "���������� � ����������������!";
            }
            else if((input > 12) && (input < 18) && ((input & 1) != 0))
            {
                congratulationText = "���������� � ��������� � ������� �����!";
            }
            else
            {
                congratulationText = "���������� � " + input.ToString() + "-������!";
            }
         
            return congratulationText;
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            double firstDouble;
            double secondDouble;
            double result;

            ParseDouble(first, out firstDouble);
            ParseDouble(second, out secondDouble);

            if((!Double.IsNaN(firstDouble)) && (!Double.IsNaN(secondDouble)))
            {
                result = firstDouble * secondDouble;
            }
            else
            {
                Console.WriteLine("Check inputed numbers! Something went wrong!");
                result = 0;
            }

            return result;
        }

        public double GetFigureValues(Figures figureType, Parameters parameterToCompute, Dimensions dimensions)
        {
            double figureValues;

            switch (figureType)
            {
                case Figures.Triangle:

                    if (parameterToCompute == Parameters.Perimeter)
                    {
                        figureValues = dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide;
                    }
                    else if (parameterToCompute == Parameters.Square)
                    {
                        figureValues = ((dimensions.FirstSide * dimensions.Height) / 2);
                    }
                    else
                    {
                        figureValues = 0;
                    }
                     
                    break;

                case Figures.Rectangle:

                    if (parameterToCompute == Parameters.Perimeter)
                    {
                        figureValues = ((dimensions.FirstSide + dimensions.SecondSide) * 2);
                    }
                    else if (parameterToCompute == Parameters.Square)
                    {
                        figureValues = (dimensions.FirstSide * dimensions.SecondSide);
                    }
                    else
                    {
                        figureValues = 0;
                    }

                    break;

                case Figures.Circle:

                    if (parameterToCompute == Parameters.Perimeter)
                    {
                        figureValues = (2 * Math.PI * dimensions.Radius);
                    }
                    else if (parameterToCompute == Parameters.Square)
                    {
                        figureValues = (Math.PI * Math.Pow(dimensions.Radius, 2));
                    }
                    else
                    {
                        figureValues = 0;
                    }

                    break;

                default:
                    figureValues = 0;

                    break;
            }

            return Math.Truncate(figureValues);
        }
    }
}